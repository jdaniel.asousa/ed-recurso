/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ArrayListPackage;

import Exceptions.*;

/**
 *
 * @author Joao
 */
public class ArrayUnorderedList<T> extends ArrayList<T> implements UnorderedListADT<T>, Iterable<T> {

    /**
     * Método construtor sem tamamnho defenido para a lista
     */
    public ArrayUnorderedList() {
        super();
    }

    /**
     * Método construtor com o tamanho para a lista definido através do
     * parametro
     *
     * @param arraySize tamanho da lista
     */
    public ArrayUnorderedList(int arraySize) {
        super(arraySize);
    }

    /**
     * Método que adiciona um certo elemento do tipo T ao front da coleção,
     * verificando se é necessário expandir o tamanho e depois fazer uma cópia
     * da coleção já existente para uma nova onde já se encontra o novo elemento
     *
     * @param element elemento a adicionar
     */
    @Override
    public void addToFront(T element) {
        if (this.size() == this.list.length) {
            this.expandCapacity();
        }

        for (int i = this.size(); i > 0; i--) {
            this.list[i] = this.list[i - 1];
        }

        this.list[0] = element;
        this.rear++;
        this.modCount++;
    }

    /**
     * Método que adiciona um certo elemento do tipo T ao rear da coleção,
     * verificando se é necessário expandir o tamanho e depois fazer adicionar o
     * elemento à posição rear da coleação
     *
     * @param element elemento a adicionar
     */
    @Override
    public void addToRear(T element) {
        if (this.size() == this.list.length) {
            this.expandCapacity();
        }

        this.list[this.rear++] = element;
        this.modCount++;
    }

    /**
     * Método que adiciona um certo elemento do tipo T logo após a outro
     * elemento, caso o mesmo exista na coleção, fazendo uma cópia de um array
     * até ao tal ElementBefore para uma nova lista, adicionar o elementAdd e
     * copiar o resto da lista, por fim substituir
     *
     * @param elementToAdd elemento a adicionar
     * @param elementBefore elemento que queremos que o elementToAdd suceda
     * @throws ElementNotFoundException if the element isn't found
     */
    @Override
    public void addAfter(T elementToAdd, T elementBefore) throws ElementNotFoundException {
        boolean found = false;
        int position = 0;

        while (!found && position < this.rear) {
            if (this.list[position].equals(elementBefore)) {
                found = true;
            } else {
                position++;
            }
        }

        if (found == false) {
            throw new ElementNotFoundException("O elemento que defeniu como antecedente ao que quer adicionar não existe.");
        }

        if (this.size() == this.list.length) {
            this.expandCapacity();
        }

        position++;

        for (int i = this.size(); i > position; i--) {
            this.list[i] = this.list[i - 1];
        }

        this.list[position] = elementToAdd;
        this.rear++;
        this.modCount++;
    }
}
