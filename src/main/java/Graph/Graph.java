/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graph;

import QueuePackage.LinkedQueue;
import ArrayListPackage.ArrayUnorderedList;
import StackPackage.LinkedStack;

import java.util.Iterator;

import Exceptions.*;

/**
 * @author Joao
 */
public class Graph<T> implements GraphADT<T> {
    protected final int DEFAULT_EXPAND_BY = 2;
    protected final int DEFAULT_CAPACITY = 10;
    protected int numVertices; // number of vertices in the graph
    protected boolean[][] adjMatrix; // adjacency matrix
    protected T[] vertices; // values of vertices

    /**
     * Constructor of a Graph with a default capacity of 10
     */
    public Graph() {
        numVertices = 0;
        this.adjMatrix = new boolean[DEFAULT_CAPACITY][DEFAULT_CAPACITY];
        this.vertices = (T[]) (new Object[DEFAULT_CAPACITY]);
    }

    /**
     * Constructor of a Graph with a capacity given by the user
     *
     * @param capacity inicial capacity of the graph
     */
    public Graph(int capacity) {
        this.numVertices = 0;
        this.adjMatrix = new boolean[capacity][capacity];
        this.vertices = (T[]) (new Object[capacity]);
    }

    /**
     * Inserts an edge between two vertices of the graph.
     *
     * @param vertex1 the first vertex
     * @param vertex2 the second vertex
     */
    @Override
    public void addEdge(T vertex1, T vertex2) {
        try {
            addEdge(getIndex(vertex1), getIndex(vertex2));
        } catch (ElementNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Inserts an edge between two vertices of the graph.
     *
     * @param index1 the first index
     * @param index2 the second index
     */
    private void addEdge(int index1, int index2) {
        if (indexIsValid(index1) && indexIsValid(index2)) {
            adjMatrix[index1][index2] = true;
            adjMatrix[index2][index1] = true;
        }
    }

    /**
     * Adds a vertex to the graph, expanding the capacity of the graph if
     * necessary. It also associates an object with the vertex.
     *
     * @param vertex the vertex to add to the graph
     */
    @Override
    public void addVertex(T vertex) {
        if (this.numVertices == this.vertices.length) {
            expandCapacity();
        }
        vertices[numVertices] = vertex;
        for (int i = 0; i <= numVertices; i++) {
            adjMatrix[numVertices][i] = false;
            adjMatrix[i][numVertices] = false;
        }
        numVertices++;
    }

    /**
     * Removes a vertex in a given index from the graph
     *
     * @param index index of the vertex to remove
     */
    private void removeVertex(int index) {
        if (this.indexIsValid(index)) {
            this.numVertices--;

            //ajuste da lista de vértices
            for (int i = index; i < this.numVertices; i++) {
                this.vertices[i] = this.vertices[i + 1];
            }

            for (int i = index; i < this.numVertices; i++) {
                for (int j = 0; j <= this.numVertices; j++) {
                    this.adjMatrix[i][j] = this.adjMatrix[i + 1][j];
                }
            }

            for (int i = index; i < this.numVertices; i++) {
                for (int j = 0; j < this.numVertices; j++) {
                    this.adjMatrix[j][i] = this.adjMatrix[j][i + 1];
                }
            }
        }
    }

    /**
     * Search for a Vertex in the graph and if exists, removes it using his
     * index
     *
     * @param vertex vertex to remove from the graph
     */
    @Override
    public void removeVertex(T vertex) throws ElementNotFoundException {
        boolean found = false;

        if (this.isEmpty()) {
            throw new ElementNotFoundException("Empty graph");
        }

        for (int i = 0; i < this.numVertices; i++) {

            if (vertex.equals(vertices[i])) {
                found = true;
                removeVertex(i);
            }
        }

        if (!found) {
            throw new ElementNotFoundException("Vertex not found in the graph.");
        }
    }

    /**
     * Removes an edge between 2 vertices in a graph using their index
     *
     * @param index1 index of the first vertex
     * @param index2 index of the second vertex
     */
    private void removeEdge(int index1, int index2) {
        if (indexIsValid(index1) && indexIsValid(index2)) {
            adjMatrix[index1][index2] = false;
            adjMatrix[index2][index1] = false;
        }
    }

    /**
     * Removes an edge between 2 vertices in a graph using the private method to
     * remove edges using index
     *
     * @param vertex1 start vertex
     * @param vertex2 target vertex
     * @throws ElementNotFoundException if the element isn't found
     */
    @Override
    public void removeEdge(T vertex1, T vertex2) throws ElementNotFoundException {
        if (this.isEmpty()) {
            throw new ElementNotFoundException("Empty graph");
        }

        removeEdge(getIndex(vertex1), getIndex(vertex2));
    }

    /**
     * Returns a breadth first iterator starting with the given vertex.
     *
     * @param startVertex the starting vertex
     * @return a breadth first iterator beginning at the given vertex
     * @throws ElementNotFoundException if the element isn't found
     */
    @Override
    public Iterator iteratorBFS(T startVertex) throws ElementNotFoundException {
        if (this.isEmpty()) {
            throw new ElementNotFoundException("Empty graph");
        }

        return iteratorBFS(getIndex(startVertex));
    }

    /**
     * Returns an iterator that performs a breadth first search traversal
     * starting at the given index.
     *
     * @param startIndex the index to begin the search from
     * @return an iterator that performs a breadth first traversal
     */
    private Iterator<T> iteratorBFS(int startIndex) {
        int x;
        LinkedQueue<Integer> traversalQueue = new LinkedQueue<Integer>();
        ArrayUnorderedList<T> resultList = new ArrayUnorderedList<T>();

        if (!indexIsValid(startIndex)) {
            return resultList.iterator();
        }

        boolean[] visited = new boolean[this.numVertices];
        for (int i = 0; i < this.numVertices; i++) {
            visited[i] = false;
        }

        traversalQueue.enqueue(startIndex);
        visited[startIndex] = true;

        while (!traversalQueue.isEmpty()) {
            try {
                x = traversalQueue.dequeue();
                resultList.addToRear(this.vertices[x]);
                /**
                 * Find all vertices adjacent to x that have not been visited and
                 * queue them up
                 */
                for (int i = 0; i < this.numVertices; i++) {
                    if (this.adjMatrix[x][i] && !visited[i]) {
                        traversalQueue.enqueue(i);
                        visited[i] = true;
                    }
                }
            } catch (EmptyCollectionException e) {
                System.out.println(e.getMessage());
            }
        }

        return resultList.iterator();
    }

    /**
     * Returns a depth first iterator starting with the given vertex.
     *
     * @param startVertex the starting vertex
     * @return a depth first iterator starting at the given vertex
     * @throws ElementNotFoundException if the element isn't found
     * @throws EmptyCollectionException if the collection is empty
     */
    @Override
    public Iterator iteratorDFS(T startVertex) throws ElementNotFoundException, EmptyCollectionException {
        if (this.isEmpty()) {
            throw new ElementNotFoundException("Empty graph");
        }

        return iteratorDFS(getIndex(startVertex));
    }

    /**
     * Returns an iterator that performs a depth first search traversal starting
     * at the given index.
     *
     * @param startIndex the index to begin the search traversal from
     * @return an iterator that performs a depth first traversal
     * @throws EmptyCollectionException if the collection is empty
     */
    private Iterator<T> iteratorDFS(int startIndex) throws EmptyCollectionException {
        int x;
        boolean found;
        LinkedStack<Integer> traversalStack = new LinkedStack<Integer>();
        ArrayUnorderedList<T> resultList = new ArrayUnorderedList<T>();
        boolean[] visited = new boolean[this.numVertices];

        if (!indexIsValid(startIndex)) {
            return resultList.iterator();
        }

        for (int i = 0; i < this.numVertices; i++) {
            visited[i] = false;
        }

        traversalStack.push(startIndex);
        resultList.addToRear(this.vertices[startIndex]);
        visited[startIndex] = true;

        while (!traversalStack.isEmpty()) {
            x = traversalStack.peek();
            found = false;
            /**
             * Find a vertex adjacent to x that has not been visited and push it
             * on the stack
             */
            for (int i = 0; (i < this.numVertices) && !found; i++) {
                if (this.adjMatrix[x][i] && !visited[i]) {
                    traversalStack.push(i);
                    resultList.addToRear(this.vertices[i]);
                    visited[i] = true;
                    found = true;
                }
            }
            if (!found && !traversalStack.isEmpty()) {
                traversalStack.pop();
            }
        }

        return resultList.iterator();
    }

    /**
     * Returns an iterator that contains the shortest path between the two
     * indexes.
     *
     * @param startIndex  the starting vertex index
     * @param targetIndex the ending vertex index
     * @return an iterator that contains the shortest path between the two
     * vertices
     * @throws EmptyCollectionException if the collection is empty
     */
    private Iterator<T> iteratorShortestPath(int startIndex, int targetIndex) throws EmptyCollectionException {
        ArrayUnorderedList<T> resultList = new ArrayUnorderedList<T>();

        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex) || (startIndex == targetIndex)) {
            return resultList.iterator();
        }

        int index = startIndex;
        int[] pathLength = new int[numVertices];
        int[] predecessor = new int[numVertices];
        LinkedQueue<Integer> traversalQueue = new LinkedQueue<Integer>();
        boolean[] visited = new boolean[numVertices];

        for (int i = 0; i < numVertices; i++) {
            visited[i] = false;
        }

        traversalQueue.enqueue(startIndex);
        visited[startIndex] = true;
        pathLength[startIndex] = 0;
        predecessor[startIndex] = -1;

        while (!traversalQueue.isEmpty() && (index != targetIndex)) {
            try {
                index = (traversalQueue.dequeue());

                /**
                 * Update the pathLength for each unvisited vertex adjacent to the
                 * vertex at the current index.
                 */
                for (int i = 0; i < this.numVertices; i++) {
                    if (this.adjMatrix[index][i] && !visited[i]) {
                        pathLength[i] = pathLength[index] + 1;
                        predecessor[i] = index;
                        traversalQueue.enqueue(i);
                        visited[i] = true;
                    }
                }
            } catch (EmptyCollectionException e) {
                System.out.println(e.getMessage());
            }
        }
        // when no path must have been found
        if (index != targetIndex) {
            return resultList.iterator();
        }

        LinkedStack<Integer> stack = new LinkedStack<Integer>();
        index = targetIndex;
        stack.push(index);
        do {
            index = predecessor[index];
            stack.push(index);
        } while (index != startIndex);

        while (!stack.isEmpty()) {
            resultList.addToRear((this.vertices[stack.pop()]));
        }

        return resultList.iterator();
    }

    /**
     * Returns an iterator that contains the shortest path between the two
     * vertices.
     *
     * @param startVertex  the starting vertex
     * @param targetVertex the ending vertex
     * @return an iterator that contains the shortest path between the two
     * vertices
     */
    @Override
    public Iterator iteratorShortestPath(T startVertex, T targetVertex) throws EmptyCollectionException, ElementNotFoundException {
        return iteratorShortestPath(getIndex(startVertex), getIndex(targetVertex));
    }

    /**
     * Method that returns true if the graphic is empty
     *
     * @return true if the graph is empty false otherwise
     */
    @Override
    public boolean isEmpty() {
        return this.numVertices == 0;
    }

    /**
     * Method that returns true if the graph is connected and false otherwise
     *
     * @return true if the graph is connected
     */
    @Override
    public boolean isConnected() {
        if (this.isEmpty()) {
            return false;
        }

        Iterator<T> itr = iteratorBFS(0);
        int count = 0;

        while (itr.hasNext()) {
            itr.next();
            count++;
        }

        return count == this.numVertices;
    }

    /**
     * Method that returns the number of vertices of the graph
     *
     * @return number of vertices of the graph
     */
    @Override
    public int size() {
        return this.numVertices;
    }

    /**
     * Method that returns the index of the first ocurrence of a vertex or
     * throws an exception if not found
     *
     * @param vertex vertex to get the index
     * @return the index of the first ocurrence of a vertex or -1 if not found
     * @throws ElementNotFoundException if the element isn't found
     */
    public int getIndex(T vertex) throws ElementNotFoundException {
        int index = -1;
        boolean found = false;
        int i = 0;

        while (i < this.numVertices && !found) {
            if (this.vertices[i].equals(vertex)) {
                found = true;
                index = i;
            }

            i++;
        }

        if (!found) {
            throw new ElementNotFoundException("Vertix not in graph");
        }

        return index;
    }

    /**
     * Method that validates if the index is valid
     *
     * @param index index to check
     * @return true if the given index is valid
     */
    public boolean indexIsValid(int index) {
        return (index > -1 && index < this.numVertices);
    }

    /**
     * Creates new array and new adj matrix with the same values as before but
     * with twice the size
     */
    protected void expandCapacity() {
        T[] tempVertices = (T[]) (new Object[this.vertices.length * this.DEFAULT_EXPAND_BY]);
        boolean[][] temp_adjMatrix = new boolean[this.vertices.length * this.DEFAULT_EXPAND_BY][this.vertices.length * this.DEFAULT_EXPAND_BY];

        for (int i = 0; i < this.vertices.length; i++) {
            tempVertices[i] = this.vertices[i];
        }

        for (int i = 0; i < this.vertices.length; i++) {
            for (int j = 0; j < this.vertices.length; j++) {
                temp_adjMatrix[i][j] = this.adjMatrix[i][j];
            }
        }

        this.vertices = tempVertices;
        this.adjMatrix = temp_adjMatrix;
    }
}
