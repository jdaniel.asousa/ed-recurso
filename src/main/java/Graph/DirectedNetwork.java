package Graph;

import ArrayListPackage.ArrayUnorderedList;

import java.util.Iterator;

import Exceptions.*;

/**
 * Directed Network Class
 *
 * @author Joao Coimbra
 * @author Daniel Sousa
 */
public class DirectedNetwork<T> extends DirectedGraph<T> implements NetworkADT<T> {
    /**
     * Variable to insert divider (not repeat every time)
     */
    private static final String DIVIDER = "========================================================\n";
    /**
     * Matrix with the corresponding weigtht
     */
    private double[][] weightMatrix;
    /**
     * String returned when the graph is empty (not repeat every time)
     */
    private static final String EMPTY = "Graph is empty";

    /**
     * Constructor method for a directed network with a capacity given by a
     * parameter
     *
     * @param capacity inicial capacity of the network
     */
    public DirectedNetwork(int capacity) {
        super(capacity);
        this.weightMatrix = new double[capacity][capacity];
        for (int i = 0; i < capacity; i++) {
            for (int j = 0; j < capacity; j++) {
                this.weightMatrix[i][j] = Double.POSITIVE_INFINITY;
            }
        }
    }

    /**
     * Inserts an edge between two vertices of the graph from v1 to v2 with a
     * specific weight
     *
     * @param vertex1 the first vertex
     * @param vertex2 the second vertex
     * @param weight  the weight of the edge
     */
    @Override
    public void addEdge(T vertex1, T vertex2, double weight) {
        super.addEdge(vertex1, vertex2);

        setEdgeWeight(vertex1, vertex2, weight);
    }

    /**
     * Method that returns the shortest path weigth between two vertices if possible -1 if not
     *
     * @param vertex1 the first vertex
     * @param vertex2 the second vertex
     * @return the shortest path weigth between two vertices if possible, -1 if not
     */
    @Override
    public double shortestPathWeight(T vertex1, T vertex2) {
        double weight = 0;

        try {
            int startIndex = this.getIndex(vertex1);
            int targetIndex = this.getIndex(vertex2);

            if (!this.indexIsValid(startIndex) || !this.indexIsValid(targetIndex)) {
                return -1;
            }

            Iterator shortestPathIterator = this.iteratorShortestPathIndices(startIndex, targetIndex);

            int index1;
            int index2;

            if (shortestPathIterator.hasNext()) {
                index1 = (int) shortestPathIterator.next();
            } else {
                return -1;
            }

            while (shortestPathIterator.hasNext()) {
                index2 = (int) shortestPathIterator.next();
                weight += this.weightMatrix[index1][index2];
                index1 = index2;
            }
        } catch (ElementNotFoundException e) {
            System.out.println(e.getMessage());
        }

        return weight;
    }

    /**
     * Method to define the weight at the weight matrix of the class
     *
     * @param vertex1 the first vertex
     * @param vertex2 the second vertex
     * @param weight  the weight of the edge
     */
    private void setEdgeWeight(T vertex1, T vertex2, double weight) {
        try {
            int index1 = super.getIndex(vertex1);
            int index2 = super.getIndex(vertex2);

            if (super.indexIsValid(index1) && super.indexIsValid(index2)) {
                this.weightMatrix[index1][index2] = weight;
            }
        } catch (ElementNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Given the start vertex index and the target index, returns an iterator with the shortest path, if possible or
     * null otherwise
     *
     * @param startIndex  start vertex index
     * @param targetIndex target vertex index
     * @return an iterator that contains the indices of the vertices that are in the shortest path between the two
     * given vertices.
     */
    private Iterator<Integer> iteratorShortestPathIndices(int startIndex, int targetIndex) {
        //lists of both visited and unvisited vertexes
        ArrayUnorderedList<Integer> visitedVertexes = new ArrayUnorderedList(this.numVertices);
        ArrayUnorderedList<Integer> unvisitedVertexes = new ArrayUnorderedList(this.numVertices);
        //previous vertex collection
        int[] previousVertex = new int[this.numVertices];
        //shortest distance from the start vertex collection
        double[] shortestDistanceFromStartVertex = new double[this.numVertices];

        //initially all the vertices aren't visited and there's no predefined previous vertex and no shortest distance
        //from  the start vertex, the last 2 will be calculated next
        for (int i = 0; i < this.numVertices; i++) {
            unvisitedVertexes.addToRear(i);
            previousVertex[i] = -1;
            shortestDistanceFromStartVertex[i] = Double.POSITIVE_INFINITY;
        }

        //exception of the distance from the start vertex to the start vertex
        shortestDistanceFromStartVertex[startIndex] = 0;
        //current vertex to compare and update the shortest path from the start vertex and the precedent vertex as well
        int currentVertex = startIndex;

        //while there's unvisited vertexes
        while (unvisitedVertexes.size() > 0) {
            //itr that contains all the neighbours vertexes from the current one
            Iterator<Integer> neighboursIndexItr = this.getAdjVertexIndices(currentVertex);

            //while there's neighbours left
            while (neighboursIndexItr.hasNext()) {
                int currentNeighbour = neighboursIndexItr.next();
                //current weight from the start vertex to the current
                double currentWeight = shortestDistanceFromStartVertex[currentVertex];

                // if the comparing weight is better than the one already chosen, then update
                if ((currentWeight + this.getEdgeWeightIndexes(currentVertex, currentNeighbour)) < shortestDistanceFromStartVertex[currentNeighbour]) {
                    shortestDistanceFromStartVertex[currentNeighbour] = currentWeight + this.getEdgeWeightIndexes(currentVertex, currentNeighbour);
                    previousVertex[currentNeighbour] = currentVertex;
                }
            }

            try {
                //update the unvisited and the visited lists
                unvisitedVertexes.remove(currentVertex);
                visitedVertexes.addToFront(currentVertex);
                //update the current vertex if there's vertex to be visited, to the closer vertex to current one
                if (!unvisitedVertexes.isEmpty()) {
                    Iterator unvisitedItr = unvisitedVertexes.iterator();
                    double comparWeight = Double.POSITIVE_INFINITY;
                    while (unvisitedItr.hasNext()) {
                        int index = (int) unvisitedItr.next();
                        if (shortestDistanceFromStartVertex[index] < comparWeight) {
                            currentVertex = index;
                            comparWeight = shortestDistanceFromStartVertex[index];
                        }
                    }
                }
            } catch (EmptyCollectionException | ElementNotFoundException e) {
                System.out.println(e.getMessage());
            }
        }

        //generate a list with the path if possible
        ArrayUnorderedList<Integer> shortestPathFromStartToTarget = new ArrayUnorderedList(this.numVertices);
        int resultIndex = targetIndex;

        while (resultIndex != startIndex) {
            if (resultIndex == -1) {
                return null;
            }
            shortestPathFromStartToTarget.addToFront(resultIndex);
            resultIndex = previousVertex[resultIndex];
        }
        shortestPathFromStartToTarget.addToFront(startIndex);

        return shortestPathFromStartToTarget.iterator();
    }

    /**
     * Given a vertex index, an iterator  with the adjency vertex's indices is returned
     *
     * @param vertexIndex actual vertex index
     * @return an iterator with the adjency vertex's indices to the given vertex
     */
    protected Iterator<Integer> getAdjVertexIndices(int vertexIndex) {
        ArrayUnorderedList<Integer> adjVertexs = new ArrayUnorderedList<Integer>();

        for (int i = 0; i < this.numVertices; i++) {
            if (this.adjMatrix[vertexIndex][i]) {
                adjVertexs.addToRear(i);
            }
        }

        return adjVertexs.iterator();
    }

    /**
     * Returns an iterator that contains the shortest path between the two
     * vertices.
     *
     * @param startVertex  the starting vertex
     * @param targetVertex the ending vertex
     * @return an iterator that contains the shortest path between the two
     * vertices
     */
    public Iterator<T> iteratorShortestPath(T startVertex, T targetVertex) {
        ArrayUnorderedList templist = new ArrayUnorderedList();

        try {
            int startIndex = getIndex(startVertex);
            int targetIndex = getIndex(targetVertex);

            if (!indexIsValid(startIndex) || !indexIsValid(targetIndex) || (startIndex == targetIndex) || isEmpty()) {
                return templist.iterator();
            }

            Iterator<Integer> it = iteratorShortestPathIndices(startIndex, targetIndex);

            while (it.hasNext()) {
                templist.addToRear(this.vertices[(it.next())]);
            }
        } catch (ElementNotFoundException e) {
            System.out.println(e.getMessage());
        }

        return templist.iterator();
    }

    /**
     * Method that returns the weight from one vertex to another if there's connection from vertex1 to vertex2
     *
     * @param vertex1 first vertex
     * @param vertex2 second vertex
     * @return the weight of an edge between 2 given edges
     */
    public double getEdgeWeight(T vertex1, T vertex2) {
        double result = -1;

        try {
            int index1 = this.getIndex(vertex1);
            int index2 = this.getIndex(vertex2);

            if (this.adjMatrix[index1][index2]) {
                result = this.weightMatrix[index1][index2];
            } else {
                throw new IllegalArgumentException("Não existe ligação entre os vértices");
            }
        } catch (ElementNotFoundException e) {
            System.out.println(e.getMessage());
        }

        return result;
    }

    /**
     * Method that returns the weight from one vertex to another if there's connection from vertex1 to vertex2
     *
     * @param index1 first vertex
     * @param index2 second vertex
     * @return the weight of an edge between 2 given edges
     */
    private double getEdgeWeightIndexes(int index1, int index2) {
        double result = -1;

        if (this.adjMatrix[index1][index2]) {
            result = this.weightMatrix[index1][index2];
        } else {
            throw new IllegalArgumentException("Não existe ligação entre os vértices");
        }

        return result;
    }

    /**
     * Returns a String representation of the adjency matrix, the weigth matrix of the network
     *
     * @return String representation of the adjency matrix, the weigth matrix of the network
     */
    public String toString() {

        if (this.numVertices == 0) {
            return EMPTY;
        }
        String result = "";

        result += getAdjacencyMatrix();

        result += "\n";

        result += getWeightMatrix();

        return result;
    }

    private String getWeightMatrix() {
        String result = "";

        if (this.numVertices == 0) {
            return EMPTY;
        }

        result += DIVIDER;
        result += "Weigth Matrix\n";
        result += DIVIDER;
        result += "index\t";

        for (int i = 0; i < this.numVertices; i++) {
            result += "" + i;
            if (i < 10) {
                result += "    ";
            }
        }
        result += "\n" + DIVIDER;

        for (int i = 0; i < this.numVertices; i++) {
            result += "" + i + "\t\t";

            for (int j = 0; j < this.numVertices; j++) {
                if (this.weightMatrix[i][j] == Double.POSITIVE_INFINITY) {
                    result += "--   ";
                } else {
                    result += this.weightMatrix[i][j] + " ";
                }
            }
            result += "\n";
        }

        return result;
    }

    private String getAdjacencyMatrix() {
        String result = "";

        if (this.numVertices == 0) {
            return EMPTY;
        }

        result += DIVIDER;
        result += "               Adjacency Matrix                \n";
        result += DIVIDER;
        result += "Index\t";

        for (int i = 0; i < this.numVertices; i++) {
            result += "" + i + " | ";
        }

        result += "Room\n" + DIVIDER;

        for (int i = 0; i < this.numVertices; i++) {
            result += "" + i + "\t\t";

            for (int j = 0; j < this.numVertices; j++) {
                if (this.adjMatrix[i][j]) {
                    result += "1" + " | ";
                } else {
                    result += "0" + " | ";
                }
            }

            result += this.vertices[i].toString() + "\n";
        }
        result += DIVIDER;
        result += "index\troom\n";
        result += DIVIDER;

        for (int i = 0; i < this.numVertices; i++) {
            result += "  " + i + "\t\t";
            result += this.vertices[i].toString() + "\n";
        }

        return result;
    }

    /**
     * Returns the vertex of a given index
     *
     * @param index index of the vertex
     *
     * @return the vertex of a given index
     */
    public T getVertex(int index){return this.vertices[index];}
}
