package Game;

/**
 * Class to storage details about the map
 *
 */
public class MapId {
    private String fileName;
    private String mapName;

    /**
     * Constructor method
     * @param file the file name
     * @param map the map name on JSON
     */
    public MapId(String file, String map) {
        this.fileName = file;
        this.mapName = map;
    }

    public String getFileName() {
        return fileName;
    }

    public String getMapName() {
        return mapName;
    }
}
