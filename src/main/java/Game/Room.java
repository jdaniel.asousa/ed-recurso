package Game;

public class Room {
    /**
     * Room name
     */
    private String roomName;
    /**
     * Ghost damage if positive, Shield heal if negative or neither of them if 0
     */
    private double damageOrHeal;

    /**
     * Constructor method for a room with a ghost
     *
     * @param roomName     Room name
     * @param damageOrHeal Ghost damage
     */
    public Room(String roomName, double damageOrHeal) {
        this.roomName = roomName;
        this.damageOrHeal = damageOrHeal;
    }

    /**
     * Constructor method for a room with no ghost
     *
     * @param roomName Room name
     */
    public Room(String roomName) {
        this.roomName = roomName;
        this.damageOrHeal = 0;
    }

    public String getRoomName() {
        return roomName;
    }

    public double getDamageOrHeal() {
        return damageOrHeal;
    }

    public void setDamageOrHeal(double damageOrHeal) {
        this.damageOrHeal = damageOrHeal;
    }

    /**
     * Overriding equals() to compare two Rooms
     *
     * @return true if the room name is the same
     */
    @Override
    public boolean equals(Object o) {
        // If the object is compared with itself then return true
        if (o == this) {
            return true;
        }

        // Check if o is an instance of Complex or not "null instanceof [type]" also returns false
        if (!(o instanceof Room)) {
            return false;
        }

        // typecast o to Room so that we can compare the room name
        Room compareRoom = (Room) o;

        return this.roomName.equals(compareRoom.getRoomName());
    }

    @Override
    public String toString() {
        return this.roomName;
    }
}
