package Game;

import ArrayListPackage.ArrayOrderedList;
import ArrayListPackage.ArrayUnorderedList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.lang.Number;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;

import Exceptions.*;


import ArrayListPackage.ArrayUnorderedList;

import static ConfigReader.ConfigReader.getNumberOfRooms;

/**
 * Class Game that has the principal implementations of the methods needed to a correct functionality of the game, this
 * includes the necessary variables as the graph that represents the map, the name, the initial health and the level
 *
 * @author Joao Coimbra
 * @author Daniel Sousa
 */
public class Game {
    /**
     * Map of the game that is an oriented network
     */
    private GameDirectedGraph<Room> map;
    /**
     * Name of the game
     */
    private String name;
    /**
     * Initial health
     */
    private double health;
    /**
     * Difficulty of the actual game
     */
    private int level;
    /**
     * Ratings
     */
    private ArrayOrderedList<Score> scores = new ArrayOrderedList<Score>();
    /**
     * default denomination of the exit on the maps
     */
    private static final Room DEFAULTEXIT = new Room("exterior");
    /**
     * default denomination of the entry on the maps
     */
    private static final Room DEFAULTENTRY = new Room("entrada");
    /**
     * Variable to do not repeat every time want use divider
     */
    private static final String DIVIDER = "========================================================";
    /**
     * Variable to do not repeat every time want use line
     */
    private static final String LINE = "―――――――――――――――――――――――――";

    /**
     *
     * Constructor method for a game
     *
     * @param mapName Map name
     * @param level   level of the actual game
     * @throws InvalidMapException when the map is invalid
     */
    public Game(String mapName, int level) throws InvalidMapException {
        this.map = new GameDirectedGraph<Room>(getNumberOfRooms(mapName));
        this.level = level;
        this.readMap(mapName);
        if (!this.map.hasPathBetween(DEFAULTENTRY, DEFAULTEXIT)) {
            throw new InvalidMapException("There's no path between the entry and the exit.\nChoose another map...");
        }
        if (this.map.shortestPathWeight(DEFAULTENTRY, DEFAULTEXIT) >= 100) {
            throw new InvalidMapException("It's impossible to reach the exit with life using the giving map, and the giving level.\nChoose another map...");
        }
        this.readRatings();
    }

    /**
     * Method that returns a string with the representation of both matrix's (adj and weights)
     *
     * @return a string with the representation of both matrix's (adj and weights)
     */
    public String getMatrix() {
        return this.map.toString();
    }

    public double getHealth() {
        return health;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    /**
     * Verifies if a vertex is in the network, if not adds it to the network
     *
     * @param vertex vertex to verify and add if needed
     */
    private void createVertex(Room vertex) {
        try {
            int roomIndex = this.map.getIndex(vertex);
            if (vertex.getDamageOrHeal() != 0) {
                this.map.getVertex(roomIndex).setDamageOrHeal(vertex.getDamageOrHeal());
            }
        } catch (ElementNotFoundException e) {
            this.map.addVertex(vertex);
        }
    }

    /**
     * Generates a directed netowrk with the correct connections by reading a json
     *
     * @param mapName Name of the json file tha has the map
     */
    private void generateNetwork(String mapName) {
        try {
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser.parse(new FileReader("Map/" + mapName + ".json"));
            JSONArray mapArray = (JSONArray) jsonObject.get("mapa");

            for (Object object : mapArray) {
                JSONObject roomInfo = (JSONObject) object;
                Room room = new Room((String) roomInfo.get("aposento"), ((Number) roomInfo.get("fantasma")).doubleValue());

                this.createVertex(room);

                JSONArray roomConnectionsArray = (JSONArray) roomInfo.get("ligacoes");
                for (Object o : roomConnectionsArray) {
                    Room connection = new Room((String) o);

                    this.createVertex(connection);

                    this.updateEdges(room, connection);
                }
            }
        } catch (IOException | ParseException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * For a given room and a given connection updates the edges values correctly
     *
     * @param room       actual room
     * @param connection room connection
     */
    private void updateEdges(Room room, Room connection) {
        if (!this.map.areConnected(room, connection)) {
            this.map.addEdge(room, connection, 0);
        }
        this.map.addEdge(connection, room, room.getDamageOrHeal() * this.level);
    }

    /**
     * Method that reads a given map and creates a directed network with it
     *
     * @param mapName Name of the json file tha has the map
     */
    private void readMap(String mapName) {
        try {
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser.parse(new FileReader("Map/" + mapName + ".json"));
            this.name = (String) jsonObject.get("nome");
            this.health = ((Number) jsonObject.get("pontos")).doubleValue();

            this.generateNetwork(mapName);
        } catch (IOException | ParseException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Returns the health left
     *
     * @param damage the actual cost of the path
     * @return the health points left
     */
    private double getHealthLeft(double damage) {
        return this.health - damage;
    }

    /**
     * Method that does the simulation mode for the actual, fives the shortest path and the points of health remaining
     * using the respective path
     */
    public void simulation() {
        Iterator shortestPathIterator = this.map.iteratorShortestPath(DEFAULTENTRY, DEFAULTEXIT);
        double shortestPathWeight = this.map.shortestPathWeight(DEFAULTENTRY, DEFAULTEXIT);

        System.out.println(LINE);
        System.out.println("Simulation Mode");
        System.out.println(LINE);
        System.out.print("| The best track to this map is : ");
        while (shortestPathIterator.hasNext()) {
            System.out.print(((Room) shortestPathIterator.next()).getRoomName());
            if (shortestPathIterator.hasNext()) {
                System.out.print(" -> ");
            }
        }
        System.out.println("\n| Using this path you reach the outside with " + getHealthLeft(shortestPathWeight) + " points of health left.");
        System.out.println(LINE);
    }

    /**
     * Method that gives the player the change of choose his path by a list of every adjency room of the current room
     * as an option to go further.This mod end's when the player reaches "exterior" or get's out of point's of health.
     * In this mod and by using an auxiliary method, the record of the play is registered in a specific json just for
     * ratings.
     *
     * @param playerName name of the player
     */
    public void manual(String playerName) {
        int shieldIndex = this.generateShield();
        double currentHealth = this.health;
        Room currentVertex = DEFAULTENTRY;
        String currentVertexName = currentVertex.getRoomName();
        Scanner scanner = new Scanner(System.in);
        ArrayUnorderedList<String> path = new ArrayUnorderedList<>();
        path.addToRear(currentVertexName);
        Room goToRoom = null;

        while (currentHealth > 0 && !currentVertex.equals(DEFAULTEXIT)) {
            System.out.println(LINE);
            System.out.println("| Current location: " + currentVertex.getRoomName() + " |");
            System.out.println("| Current health: " + currentHealth + "|");
            System.out.println(LINE);
            while (goToRoom == null) {
                System.out.println("Go to: ");
                Iterator itrAdjVertexs = this.map.getAdjVertexs(currentVertex);
                int count = 1;

                while (itrAdjVertexs.hasNext()) {
                    Room connection = (Room) itrAdjVertexs.next();
                    if (!connection.equals(DEFAULTENTRY)) {
                        System.out.println(count++ + ") " + connection.getRoomName());
                    }
                }

                System.out.println(LINE);
                System.out.println("Chose one of the giving rooms:");
                String goToIndex = scanner.nextLine();

                if (!goToIndex.matches("[1-" + count + "]")) {
                    System.out.println("Invalid option");
                } else {
                    Iterator getGoToItr = this.map.getAdjVertexs(currentVertex);
                    int count2 = 0;
                    while (getGoToItr.hasNext()) {
                        Room connection = (Room) getGoToItr.next();
                        if (!connection.equals(DEFAULTENTRY)) {
                            if ((++count2) == Integer.parseInt(goToIndex)) {
                                goToRoom = connection;
                            }
                        }
                    }
                    path.addToRear(goToRoom.getRoomName());
                    currentHealth -= this.map.getEdgeWeight(currentVertex, goToRoom);
                    //special situation for when the room contains a shield
                    if (this.map.getEdgeWeight(currentVertex, goToRoom) < 0) {
                        System.out.println(LINE);
                        System.out.println("Nice you found a shield that restores " + (-1 * this.map.getEdgeWeight(currentVertex, goToRoom)) + " points of health");
                        this.removeShield(shieldIndex);
                    }
                    currentVertex = goToRoom;
                }
            }
            goToRoom = null;
        }
        if (currentHealth > 0) {
            System.out.println(DIVIDER);
            System.out.println("Victory");
            System.out.print("\nPoints of health left: " + currentHealth);

        } else {
            System.out.println("Buhhhhhhh you are a looser...");
            currentHealth = 0;
        }
        System.out.print("\nPath: ");
        String pathStr = "";
        Iterator pathIterator = path.iterator();

        while (pathIterator.hasNext()) {
            pathStr += pathIterator.next();
            if (pathIterator.hasNext()) {
                pathStr += " -> ";
            }
        }

        System.out.println(pathStr);
        System.out.println("\n" + DIVIDER);

        try {
            this.scores.add(new Score(pathStr, currentHealth, playerName));
        } catch (NotComparableException e) {
            System.out.println(e.getMessage());
        }

        this.removeShield(shieldIndex);
    }

    /**
     * When the software is closed, the ratings in the json file are updated by the one's
     */
    protected void writeRatingJSON() {
        String ratingsPath = "Ratings/ratings.json";
        File file = new File(ratingsPath);

        //create the map json element
        JSONObject map = new JSONObject();
        JSONArray scores = new JSONArray();

        map.put("mapName", this.name);
        map.put("level", this.level);
        map.put("initialHealth", this.health);

        Iterator scoresItr = this.scores.iterator();

        while (scoresItr.hasNext()) {
            JSONObject scoreJson = new JSONObject();
            Score score = (Score) scoresItr.next();
            scoreJson.put("player", score.getPlayerName());
            scoreJson.put("health", score.getHealth());
            scoreJson.put("path", score.getPath());

            scores.add(scoreJson);
        }

        map.put("scores", scores);

        // if the file doesn't exist
        if (!file.exists()) {
            try (FileWriter write = new FileWriter(ratingsPath)) {
                JSONObject ratings = new JSONObject();
                JSONArray maps = new JSONArray();

                maps.add(map);
                ratings.put("maps", maps);

                write.write(ratings.toJSONString());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        } else {
            try {
                JSONParser parser = new JSONParser();
                JSONObject ratings = (JSONObject) parser.parse(new FileReader(ratingsPath));
                JSONArray maps = (JSONArray) ratings.get("maps");
                boolean found = false;

                for (Object obj : maps) {
                    JSONObject mapRating = (JSONObject) obj;
                    int level = ((Number) mapRating.get("level")).intValue();

                    // search for the element that has the same name, same difficulty and same initial health as the one
                    // that we want to add
                    if (mapRating.get("mapName").equals(this.name) && mapRating.get("initialHealth").equals(this.health) && level == this.level) {
                        found = true;

                        mapRating.remove("scores");
                        mapRating.put("scores", scores);
                    }
                }
                //if there's no ratings  for this map, difficulty and initial health
                if (!found) {
                    maps.add(map);
                }
                //write the json object updated in the file
                try (FileWriter write = new FileWriter(ratingsPath)) {
                    write.write(ratings.toJSONString());
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            } catch (IOException | ParseException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * Method that displays the map information. The rooms, the connections and the information of a ghost in each room
     * of a current map
     */
    public void displayMap() {
        System.out.println(DIVIDER);
        System.out.println("\t\t\t\t\tMap view");
        System.out.println(DIVIDER);

        try {
            Iterator<Room> bfsItr = this.map.iteratorBFS(DEFAULTENTRY);
            while (bfsItr.hasNext()) {
                Room current = bfsItr.next();
                System.out.println("The room '" + current.getRoomName() + "' has connections to:");

                Iterator<Room> connectionsItr = this.map.getAdjVertexs(current);

                while (connectionsItr.hasNext()) {
                    String adjRoom = connectionsItr.next().getRoomName();
                    System.out.println("- '" + adjRoom + "'");
                }

                if (current.getDamageOrHeal() > 0) {
                    System.out.println("Careful, '" + current.getRoomName() + "' has a ghost that causes " + current.getDamageOrHeal() + " points of damage!");
                } else {
                    System.out.println("Here in '" + current.getRoomName() + "' you are safe, there's no ghost, but there's no shield either.");
                }

                System.out.println(LINE);
            }
        } catch (ElementNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Method to generate the shield protection in one vertex that doesn't have ghost, that protection will be a number
     * between and return the index of it
     *
     * @return the index where the shield is generated
     */
    private int generateShield() {
        boolean protectionAdded = false;
        Random r = new Random();
        int randomRoomIndex = -1;

        while (!protectionAdded) {
            randomRoomIndex = r.nextInt(this.map.size());

            if (!this.map.getVertex(randomRoomIndex).equals(DEFAULTENTRY) && !this.map.getVertex(randomRoomIndex).equals(DEFAULTEXIT) && (this.map.getVertex(randomRoomIndex).getDamageOrHeal() == 0)) {
                int healValue = 0 - r.nextInt((int) this.getMaxDamageInMap() - 1) + 1;
                this.map.getVertex(randomRoomIndex).setDamageOrHeal(healValue);

                Iterator neighboursItr = this.map.getAdjVertexs(this.map.getVertex(randomRoomIndex));

                while (neighboursItr.hasNext()) {
                    this.updateEdges(this.map.getVertex(randomRoomIndex), (Room) neighboursItr.next());
                }


                System.out.println("Shield added to the " + this.map.getVertex(randomRoomIndex) + " with the value " + healValue);

                protectionAdded = true;
            }
        }

        return randomRoomIndex;
    }

    /**
     * Returns the max damage cause by a ghost in the current map
     *
     * @return max damage cause by a ghost in the current map
     */
    private double getMaxDamageInMap() {
        double maxDamage = -1;

        for (int i = 0; i < this.map.size(); i++) {
            if (this.map.getVertex(i).getDamageOrHeal() > maxDamage) {
                maxDamage = this.map.getVertex(i).getDamageOrHeal();
            }
        }

        return (maxDamage / this.level);
    }

    /**
     * Method used initially when the map and the level is chosen, to read the json with the ratings
     */
    private void readRatings() {
        try {
            JSONParser parser = new JSONParser();

            // Object to get scores
            JSONObject jsonObject = (JSONObject) parser.parse(new FileReader("Ratings/ratings.json"));
            JSONArray mapRatings = (JSONArray) jsonObject.get("maps");

            for (Object obj : mapRatings) {
                JSONObject mapRating = (JSONObject) obj;
                int level = ((Number) mapRating.get("level")).intValue();
                double initialHealth = ((Number) mapRating.get("initialHealth")).doubleValue();

                if (mapRating.get("mapName").equals(this.getName()) && this.getLevel() == level && this.getHealth() == initialHealth) {
                    JSONArray scores = (JSONArray) mapRating.get("scores");

                    for (Object scoresAvaible : scores) {
                        JSONObject score = (JSONObject) scoresAvaible;

                        try {
                            this.scores.add(new Score((String) score.get("path"), (Double) score.get("health"), (String) score.get("player")));
                        } catch (NotComparableException e) {
                            System.out.println(e.getMessage());
                        }
                    }
                }
            }
        } catch (IOException | ParseException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Method to display the ratings of the map
     */
    public void displayRatings() {
        boolean found = false;
        Iterator ratingsItr = this.scores.iterator();
        String result = "";

        result += LINE + "\n";
        result += "Map Name: " + this.name + "\n";
        result += "Difficulty: " + this.level + "\n";
        result += "Initial Health: " + this.health + "\n";
        result += LINE + "\n";

        while (ratingsItr.hasNext()) {
            Score printScore = (Score) ratingsItr.next();
            found = true;

            result += "| Player " + printScore.getPlayerName() + "\n";
            result += "| Path: " + printScore.getPath() + "\n";
            result += "| Health: " + printScore.getHealth() + "\n";
            result += LINE + "\n";
        }

        if (found) {
            System.out.println(result);
        } else {
            System.out.println("There's no ratings yet for this map.");
        }
    }

    /**
     * Private method that removes the shield from the map using its index.
     * This method will be used when the shield is catch or the play ends and the shield wasn't found
     *
     * @param shieldIndex shield index
     */
    private void removeShield(int shieldIndex) {
        this.map.getVertex(shieldIndex).setDamageOrHeal(0);

        Iterator neighboursItr = this.map.getAdjVertexs(this.map.getVertex(shieldIndex));

        while (neighboursItr.hasNext()) {
            this.updateEdges(this.map.getVertex(shieldIndex), (Room) neighboursItr.next());
        }
    }
}
