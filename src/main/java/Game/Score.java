package Game;

public class Score implements Comparable<Score> {
    /**
     * The path of the rating
     */
    private String path;
    /**
     * Health left from the play
     */
    private double health;
    /**
     * Player name
     */
    private String playerName;

    /**
     * Constructor method for a score
     *
     * @param path       full path
     * @param health     health left
     * @param playerName player name
     */
    public Score(String path, double health, String playerName) {
        this.path = path;
        this.health = health;
        this.playerName = playerName;
    }

    public String getPath() {
        return path;
    }

    public double getHealth() {
        return health;
    }

    public String getPlayerName() {
        return playerName;
    }

    /**
     * Used to sort scores by health
     *
     * @param s score to compare
     * @return if the health of the given score is higher, less then 0 or  plus then 0 otherwise
     */
    public int compareTo(Score s) {
        return (int) (this.health - s.health);
    }
}
