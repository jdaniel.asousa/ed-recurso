/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QueuePackage;

import Exceptions.*;

/**
 *
 * @author Joao
 */
public class LinkedQueue<T> implements QueueADT<T> {

    /**
     * Number of elements in the queue
     */
    private int count;
    /**
     * front node reference and rear node reference
     */
    private LinearNode<T> front, rear;

    /**
     * Constructor method
     */
    public LinkedQueue() {
        this.count = 0;
        this.front = null;
        this.rear = null;
    }

    /**
     * Method that adds an ellement to the rear of the queue
     *
     * @param element the element to be added to the rear of this queue
     */
    @Override
    public void enqueue(T element) {
        LinearNode<T> addNode = new LinearNode<T>(element);
        
        
        if (this.count == 0) {
            this.front = addNode;
            this.rear = this.front;
        } else {
            this.rear.setNext(addNode);
            this.rear = addNode;
        }
        
        this.count++;
    }

    /**
     * Method that removes if possible the front Node from the Queue
     *
     * @return The element from the node referenced in the front of the queue
     *
     * @throws EmptyCollectionException if the collection is empty
     */
    @Override
    public T dequeue() throws EmptyCollectionException {
        LinearNode<T> removedNode;
        if (this.count == 0) {
            throw new EmptyCollectionException("A Queue encontra-se vazia.");
        }

        if (this.count == 1) {
            removedNode = this.front;
            this.front = null;
            this.rear = null;
            removedNode.setNext(null);
        } else {
            removedNode = this.front;
            this.front = this.front.getNext();
            removedNode.setNext(null);
        }
        
        this.count--;
        
        return removedNode.getElement();
    }

    /**
     * Method that returns the element in the front of the queue
     *
     * @return the element in the front of the queue
     * @throws EmptyCollectionException if the collection is empty
     */
    @Override
    public T first() throws EmptyCollectionException {
        if (this.count == 0) {
            throw new EmptyCollectionException("A Queue encontra-se vazia.");
        } else {
            return this.front.getElement();
        }
    }

    /**
     * Returns true if this queue contains no elements.
     *
     * @return true if this queue is empty
     */
    public boolean isEmpty() {
        return this.count == 0;
    }

    /**
     * Rerturn the number of elements in this queue
     *
     * @return the number of elements in the queue
     */
    public int size() {
        return this.count;
    }

    /**
     * Returns a string representation of this queue.
     *
     * @return the string representation of this queue
     */
    public String toString() {
        LinearNode<T> current = this.front;
        String r = "";
        r += "LinkedStack\n" + "count=" + count;
        while (current != null) {
            r += current.toString();
            current = current.getNext();
        }
        return r;
    }
}
