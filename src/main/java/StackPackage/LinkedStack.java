/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StackPackage;

import Exceptions.*;

/**
 *
 * @author Joao
 */
public class LinkedStack<T> implements StackADT<T> {

    /**
     * Number of elements on the stack
     */
    private int count;
    /**
     * Last Node added to the stack
     */
    private LinearNode top;

    /**
     * Constructor with the first element in it, so the count is already on one
     *
     * @param element element to start the stack with
     */
    public LinkedStack(T element) {
        this.count = 1;
        LinearNode newTop = new LinearNode(element);
        this.top = top;
    }

    /**
     * Constructor for a linked stack with no nodes in it
     */
    public LinkedStack() {
        this.count = 0;
        this.top = null;
    }

    /**
     * Adds the specified element to the top of this stack, expanding the the
     * count variable.
     *
     * @param element generic element to be pushed onto stack
     */
    @Override
    public void push(T element) {
        LinearNode newTop = new LinearNode(element);
        if (this.top == null) {
            this.top = newTop;
            this.count++;
        } else {
            newTop.setNext(this.top);
            this.top = newTop;
            this.count++;
        }
    }

    /**
     * Removes the element at the top of this stack and returns a reference to
     * it. Throws an EmptyCollectionException if the stack is empty.
     *
     * @return T element removed from top of stack
     * @throws EmptyCollectionException if a pop is attempted on empty stack
     */
    @Override
    public T pop() throws EmptyCollectionException {
        LinearNode popNode;
        if (size() == 0) {
            throw new EmptyCollectionException("Stack vazia");
        } else {
            popNode = this.top;
            this.top = this.top.getNext();
            popNode.setNext(null);
            this.count--;
            return (T) popNode.getElement();
        }
    }

    /**
     * Returns a reference to the element at the top of this stack. The element
     * is not removed from the stack. Throws an EmptyCollectionException if the
     * stack is empty.
     *
     * @return T element on top of stack
     * @throws EmptyCollectionException if a peek is attempted on empty stack
     */
    public T peek() throws EmptyCollectionException {
        return (T) top.getElement();
    }

    /**
     * Returns true if this stack contains no elements.
     *
     * @return boolean whether or not this stack is empty
     */
    @Override
    public boolean isEmpty() {
        if (size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns the number of elements in this stack.
     *
     * @return int number of elements in this stack
     */
    @Override
    public int size() {
        return this.count;
    }

    @Override
    public String toString() {
        return "LinkedStack{" + "count=" + count + ", top=" + top + '}';
    }

}
